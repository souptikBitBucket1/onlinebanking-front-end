import { Component, OnInit, ElementRef } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import {UserService} from "../../services/user/user.service";
import {User} from "../../entity/user";
import {Utils} from "../../Utils/utils";
import {UtilService} from "../../services/util.service";

declare var $: any;

@Component({
    selector: 'app-login-cmp',
    templateUrl: './login.component.html'
})

export class LoginComponent implements OnInit {
    loader: boolean = false;

    private loginForm: FormGroup;
    /*private name: string;*/
    email: string;
    password: string;

    user: User;
    msg: string;

    test: Date = new Date();
    private toggleButton: any;
    private sidebarVisible: boolean;
    private nativeElement: Node;

    constructor(private element: ElementRef,
                private router: Router,
                private userService: UserService,
                private utilService: UtilService) {
        this.nativeElement = element.nativeElement;
        this.sidebarVisible = false;
    }

    ngOnInit() {
        var navbar : HTMLElement = this.element.nativeElement;
        this.toggleButton = navbar.getElementsByClassName('navbar-toggle')[0];
        setTimeout(function() {
            // after 1000 ms we add the class animated to the login/register card
            $('.card').removeClass('card-hidden');
        }, 700);

        // form validation
        this.loginForm = new FormGroup({
            /*name: new FormControl(),*/
            email: new FormControl(),
            password: new FormControl()
        });

    }
    sidebarToggle() {
        var toggleButton = this.toggleButton;
        var body = document.getElementsByTagName('body')[0];
        var sidebar = document.getElementsByClassName('navbar-collapse')[0];
        if (this.sidebarVisible == false) {
            setTimeout(function() {
                toggleButton.classList.add('toggled');
            }, 500);
            body.classList.add('nav-open');
            this.sidebarVisible = true;
        } else {
            this.toggleButton.classList.remove('toggled');
            this.sidebarVisible = false;
            body.classList.remove('nav-open');
        }
    }

    login(){
        this.loader = true;
        const params = {email: this.email, password: this.password};
        this.password = btoa(btoa(this.password)).slice(0,5);
        this.userService.login(params).subscribe(loginResponse => {
            if(loginResponse.status === 'success'){
                this.user = loginResponse.data;
                this.utilService.setLoginResponse(this.user);
                this.router.navigate(['/dashboard']);
            }else{
                this.msg = 'Wrong Credentials!!!';
                setTimeout(() => {
                    this.msg = '';
                }, 3000);
            }

            this.loader = false;
        });
    }
}
