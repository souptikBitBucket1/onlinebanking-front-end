import {AfterViewInit, Component, OnInit} from '@angular/core';
import {TableData} from '../md/md-table/md-table.component';

import * as Chartist from 'chartist';
import {User} from '../entity/user';
import {UtilService} from '../services/util.service';
import {UserService} from '../services/user/user.service';

declare const $: any;

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html'
})
export class DashboardComponent implements OnInit, AfterViewInit {
    user: User;
    userId: number;
    constructor(private utilService: UtilService,
                private userService: UserService){}

    public ngOnInit() {
        this.user = this.utilService.getLoginResponse();

        this.userService.getUserById(this.user.id).subscribe(response => {
            if(response.status === 'success'){
                this.user = response.data;
                this.utilService.setLoginResponse(this.user);
            }else{
                this.utilService.deleteLoginResponse();
                window.location.reload();
            }
        });
    }
    ngAfterViewInit() {
    }
}
