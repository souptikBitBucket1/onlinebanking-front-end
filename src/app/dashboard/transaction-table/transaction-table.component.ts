import {Component, Input, OnInit} from '@angular/core';
import {TransactionService} from '../../services/transaction/transaction.service';
import {User} from '../../entity/user';

@Component({
    selector: 'app-transaction-table',
    templateUrl: './transaction-table.component.html',
    styleUrls: ['./transaction-table.component.css']
})
export class TransactionTableComponent implements OnInit {

    @Input('user')
    user: User;

    rows = [];

    constructor(private transactionService: TransactionService) { }

    ngOnInit() {
      if(!!this.user){
          this.transactionService.getTransactionByUserId(this.user.id).subscribe(response => {
                let row;
              for(let transaction of response.data.content){
                    if(this.user.id === transaction.creditedUserAccount.id){
                        row = {
                            creditedAmount: transaction.amount.toFixed(2),
                            debitedAmount: '',
                            transactionType: 'From Transfer',
                            beneficiaryAccount: transaction.debitedUserAccount,
                            date: new Date(transaction.timeStamp)
                        };
                    }else{
                        row = {
                            creditedAmount: '',
                            debitedAmount: transaction.amount.toFixed(2),
                            transactionType: 'To Transfer',
                            beneficiaryAccount: transaction.creditedUserAccount,
                            date: new Date(transaction.timeStamp)
                        };
                    }


                    //populate transaction table
                  if(this.rows.length <= 0){
                        this.rows = [row];
                  }else{
                        this.rows.push(row);
                  }
              }

          });
      }
    }

}
