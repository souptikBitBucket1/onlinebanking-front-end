import { Component, OnInit } from '@angular/core';
import {UtilService} from '../services/util.service';
import {FundTransferService} from '../services/fund-transfer/fund-transfer.service';
import {User} from '../entity/user';
import {Transaction} from '../entity/Transaction';
import {Router} from '@angular/router';
import {Beneficiary} from '../entity/beneficiary';
import {validate} from 'codelyzer/walkerFactory/walkerFn';
import {UserService} from '../services/user/user.service';

@Component({
    selector: 'app-fund-transfer',
    templateUrl: './fund-transfer.component.html',
    styleUrls: ['./fund-transfer.component.css']
})
export class FundTransferComponent implements OnInit {

    loader: boolean = true;

    beneficiaries: Beneficiary[];
    user: User;
    transaction: Transaction;

    userAccountNo: number;
    beneficiaryName: string;
    beneficiaryAccountNo: number;
    beneficiaryTransferLimit: number;

    transferAmount: number;


    msg: string = '';

    constructor(private utilService: UtilService,
                private userService: UserService,
                private fundTransferService: FundTransferService,
                private router: Router) {
        this.user = this.utilService.getLoginResponse();
        this.getUserById(this.user.id);
        this.userAccountNo = this.user.id;
    }

    ngOnInit() {
    }

    getUserById(userAccountNo: number){
        this.userService.getUserById(userAccountNo).subscribe(response => {
            if(response.status === 'success'){
                this.user = response.data;
            }else{
                this.utilService.deleteLoginResponse();
                window.location.reload();
            }
        });
    }

    validateTransferAmount(){
        if(this.transferAmount > this.user.balance){
            this.msg = 'Low Balance!!! Could not process transaction';
        }else{
            this.msg = '';
        }

        if(this.transferAmount > this.beneficiaryTransferLimit){
            this.msg = 'Transfer amount should not exceed beneficiary transfer limit';
        }else{
            this.msg = '';
        }
    }

    doTransfer(){
        this.loader = true;

        const transferPayload = {
            userAccountNo: this.user.id,
            beneficiaryAccountNo: this.beneficiaryAccountNo,
            transferAmount: this.transferAmount
        };
        this.fundTransferService.doTransfer(transferPayload).subscribe(response => {
            console.log(response);
            this.loader = false;
            if(response.status === 'success'){
                this.transaction = response.data;
                this.utilService.setLoginResponse(this.transaction.debitedUserAccount);
                this.router.navigate(['/dashboard']);
            }else{
                this.msg = response.msg;
            }
        });
    }

    selectBeneficiary(row){
        console.log('fund transfer', row);
        this.beneficiaryAccountNo = row.benefeciaryUser.id;
        this.beneficiaryName = row.benefeciaryUser.name;
        this.beneficiaryTransferLimit = row.transferLimit;
    }

    dataFetch(beneficiaries){
        if(beneficiaries.length > 0){
            this.loader = false;
            this.beneficiaries = beneficiaries;
        }else{
            this.loader = true;
        }
    }
}
