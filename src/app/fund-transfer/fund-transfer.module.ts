import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FundTransferComponent } from './fund-transfer.component';
import {FormsModule} from '@angular/forms';
import {MdModule} from '../md/md.module';
import {MaterialModule} from '../app.module';
import {RouterModule} from '@angular/router';
import {FundTransferRoutes} from './fund-transfer.routing';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {BeneficiaryModule} from '../beneficiary/beneficiary.module';

@NgModule({
  imports: [
    CommonModule,
      FormsModule,
      MdModule,
      MaterialModule,
      RouterModule.forChild(FundTransferRoutes),
      NgxDatatableModule,
      BeneficiaryModule
  ],
  declarations: [FundTransferComponent]
})
export class FundTransferModule { }
