import {Routes} from '@angular/router';
import {FundTransferComponent} from './fund-transfer.component';

export const FundTransferRoutes: Routes = [
    {
        path: '',
        children: [
            {
                path: 'fund-transfer',
                component: FundTransferComponent
            }
        ]
    }
];