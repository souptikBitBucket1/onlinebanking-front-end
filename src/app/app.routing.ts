import { Routes } from '@angular/router';

import { AdminLayoutComponent } from './layouts/admin/admin-layout.component';
import { AuthLayoutComponent } from './layouts/auth/auth-layout.component';
import {IndexComponent} from "./layouts/index/index.component";
import {FundTransferModule} from './fund-transfer/fund-transfer.module';

export const AppRoutes: Routes = [
    {
        path: '',
        redirectTo: 'index',
        pathMatch: 'full',
    }, {
        path: '',
        component: AdminLayoutComponent,
        children: [
            {
                path: '',
                loadChildren: './dashboard/dashboard.module#DashboardModule'
            },{
                path: 'beneficiary',
                loadChildren: './beneficiary/beneficiary.module#BeneficiaryModule'
            },{
                path: '',
                loadChildren: './fund-transfer/fund-transfer.module#FundTransferModule'
            }, {
                path: 'components',
                loadChildren: './components/components.module#ComponentsModule'
            }, {
                path: 'forms',
                loadChildren: './forms/forms.module#Forms'
            }, {
                path: 'tables',
                loadChildren: './tables/tables.module#TablesModule'
            }, {
                path: 'maps',
                loadChildren: './maps/maps.module#MapsModule'
            }, {
                path: 'widgets',
                loadChildren: './widgets/widgets.module#WidgetsModule'
            }, {
                path: 'charts',
                loadChildren: './charts/charts.module#ChartsModule'
            }, {
                path: 'calendar',
                loadChildren: './calendar/calendar.module#CalendarModule'
            }, {
                path: '',
                loadChildren: './userpage/user.module#UserModule'
            }, {
                path: '',
                loadChildren: './timeline/timeline.module#TimelineModule'
            }
        ]}, {
        path: '',
        component: AuthLayoutComponent,
        children: [{
            path: '',
            loadChildren: './auth/auth.module#AuthModule'
        }]
    },{
        path: 'index',
        component: IndexComponent
    }
];
