import { Injectable } from '@angular/core';
import {BaseService} from "../base/base.service";
import {Utils} from "../../Utils/utils";

@Injectable()
export class BeneficiaryService {
  private endpoint = Utils.HOST + 'beneficiary';

  constructor(private baseService: BaseService) { }

  addBeneficiary(body: object){
      return this.baseService.doPost(this.endpoint, body);
  }

  getBeneficiaryByUserId(userId: number){
      return this.baseService.doGet(this.endpoint + '/' + userId);
  }

  deleteBeneficiary(beneficiaryId: number){
      return this.baseService.doDelete(this.endpoint + '/' + beneficiaryId);
  }
}
