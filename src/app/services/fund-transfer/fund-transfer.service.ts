import { Injectable } from '@angular/core';
import {BaseService} from '../base/base.service';
import {Utils} from '../../Utils/utils';

@Injectable()
export class FundTransferService {

    private endpoint: string = Utils.HOST + 'transaction';
    constructor(private baseService: BaseService) { }

    doTransfer(body: object){
        return this.baseService.doPost(this.endpoint, body);
    }
}
