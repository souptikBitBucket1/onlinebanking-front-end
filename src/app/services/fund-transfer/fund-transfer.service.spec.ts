import { TestBed, inject } from '@angular/core/testing';

import { FundTransferService } from './fund-transfer.service';

describe('FundTransferService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FundTransferService]
    });
  });

  it('should be created', inject([FundTransferService], (service: FundTransferService) => {
    expect(service).toBeTruthy();
  }));
});
