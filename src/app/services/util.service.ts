import { Injectable } from '@angular/core';
import {Utils} from "../Utils/utils";
import {User} from "../entity/user";

@Injectable()
export class UtilService {

    constructor() { }

    setLoginResponse(user: User){
        sessionStorage.setItem(Utils.USER_RESPONSE, btoa(JSON.stringify(user)));
    }

    getLoginResponse(){
        return JSON.parse(atob(sessionStorage.getItem(Utils.USER_RESPONSE)));
    }

    deleteLoginResponse(){
        sessionStorage.removeItem(Utils.USER_RESPONSE);
    }
}
