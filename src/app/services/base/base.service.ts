import { Injectable } from '@angular/core';
import {Http, RequestOptions} from "@angular/http";

@Injectable()
export class BaseService {

    constructor(private http: Http) { }

    doGet(url: string, params?: object){
        let options;
        if(!!params){
            options = new RequestOptions({params: params});
        }
        return this.http.get(url, options).map(res => res.json());
    }
    doPost(url: string, body: object){
        return this.http.post(url, body).map(res => res.json());
    }
    doPut(url: string, body: object){
        return this.http.put(url, body).map(res => res.json());
    }
    doDelete(url: string, body?: object){
        let options;
        if(!!body){
            options = new RequestOptions({body: body});
        }
        return this.http.delete(url, options);
    }
}
