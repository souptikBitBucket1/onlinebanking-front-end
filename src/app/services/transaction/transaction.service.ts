import { Injectable } from '@angular/core';
import {BaseService} from '../base/base.service';
import {Utils} from '../../Utils/utils';

@Injectable()
export class TransactionService {
    private endpoint = Utils.HOST + 'transaction'
    constructor(private baseService: BaseService) { }

    getTransactionByUserId(userId: number){
        return this.baseService.doGet(this.endpoint + '/' + userId);
    }

}
