import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import {Utils} from "../../Utils/utils";
import {BaseService} from "../base/base.service";

@Injectable()
export class UserService {
    endpoint: string = Utils.HOST + 'user';
    constructor(private baseService: BaseService) { }

    login(params: object){
        return this.baseService.doGet(this.endpoint, params);
    }

    getUserById(userAccountNo: number){
        return this.baseService.doGet(this.endpoint + '/' + userAccountNo);
    }
}