import { Component, OnInit } from '@angular/core';
import {UtilService} from "../../services/util.service";
import {BeneficiaryService} from "../../services/beneficiary/beneficiary.service";
import {Router} from '@angular/router';

@Component({
    selector: 'app-add-beneficiary',
    templateUrl: './add-beneficiary.component.html',
    styleUrls: ['./add-beneficiary.component.css']
})
export class AddBeneficiaryComponent implements OnInit {

    userAccountNo: number;
    beneficiaryName: string;
    beneficiaryAccountNo: number;
    branchCode: number;
    transferLimit: number;

    userLoginResponse: object;
    testVar: string;

    constructor(private utilService: UtilService,
                private beneficiaryService: BeneficiaryService,
                private router: Router) {
        this.userLoginResponse = this.utilService.getLoginResponse();
        this.userAccountNo = this.userLoginResponse['id'];
    }

    ngOnInit() {
    }

    addBeneficiary(){
        console.log(this.userAccountNo, this.beneficiaryName, this.beneficiaryAccountNo, this.branchCode);
        const body = {
            userAccountNo: this.userAccountNo,
            beneficiaryName: this.beneficiaryName,
            beneficiaryAccountNo: this.beneficiaryAccountNo,
            branchCode: this.branchCode,
            transferLimit: this.transferLimit
        };
        this.beneficiaryService.addBeneficiary(body).subscribe(response => {
            console.log(response);
            if(response['status'] === 'success'){
                this.router.navigate(['/beneficiary/manage']);
            }
            else{
                console.log(response['status'], response['msg']);
            }
        });
    }
}
