import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'app-manage-beneficiary',
    templateUrl: './manage-beneficiary.component.html',
    styleUrls: ['./manage-beneficiary.component.css']
})
export class ManageBeneficiaryComponent implements OnInit {

    loader: boolean = true;

    constructor(){

    }

    ngOnInit(){

    }

    dataFetch(rows){
        console.log(rows);
        this.loader = false;
    }
}
