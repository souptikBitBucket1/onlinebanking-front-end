import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddBeneficiaryComponent } from './add-beneficiary/add-beneficiary.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MdModule} from "../md/md.module";
import {MaterialModule} from "../app.module";
import {RouterModule} from "@angular/router";
import {BeneficiaryRoutes} from "./beneficiary.routing";
import { ManageBeneficiaryComponent } from './manage-beneficiary/manage-beneficiary.component';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import { BeneficiaryTableComponent } from './beneficiary-table/beneficiary-table.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        MdModule,
        MaterialModule,
        RouterModule.forChild(BeneficiaryRoutes),
        ReactiveFormsModule,
        NgxDatatableModule
    ],
    declarations: [AddBeneficiaryComponent, ManageBeneficiaryComponent, BeneficiaryTableComponent],
    exports: [BeneficiaryTableComponent]
})
export class BeneficiaryModule { }
