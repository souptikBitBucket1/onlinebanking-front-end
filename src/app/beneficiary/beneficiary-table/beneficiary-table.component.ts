import {AfterViewInit, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {User} from '../../entity/user';
import {UtilService} from '../../services/util.service';
import {BeneficiaryService} from '../../services/beneficiary/beneficiary.service';

declare const $: any;
declare const swal: any;

@Component({
  selector: 'app-beneficiary-table',
  templateUrl: './beneficiary-table.component.html',
  styleUrls: ['./beneficiary-table.component.css']
})
export class BeneficiaryTableComponent implements OnInit, AfterViewInit {

    @Input('isSelectBtn') isSelectBtn: boolean = false;
    @Input('isDeleteBtn') isDeleteBtn: boolean = false;

    user: User;
    rows = [];

    @Output() dataFetch: EventEmitter<any> = new EventEmitter<any>();
    @Output() selectBeneficiary: EventEmitter<any> = new EventEmitter<any>();

    constructor(private beneficiaryService: BeneficiaryService,
                private utilService: UtilService) {
        this.user = utilService.getLoginResponse();
    }

    ngOnInit() {
    }

    ngAfterViewInit(){
        this.getBeneficiary();
    }

    getBeneficiary(){
        this.beneficiaryService.getBeneficiaryByUserId(this.user.id).subscribe(response => {
            this.rows = [];
            this.rows = response;
            this.dataFetch.emit(this.rows);
        });
    }

    deleteBeneficiary(row){
        const that = this;
        this.beneficiaryService.deleteBeneficiary(row.id).subscribe(response => {
            for(let i = 0 ; i < this.rows.length ; i++){
                if(that.rows[i].id === row.id){
                    this.rows.splice(i,1);
                }
            }
        });
    }

    doSelectBeneficiary(row){
        this.selectBeneficiary.emit(row);
    }

    deleteAlert(row) {
        const that = this;
        swal({
            title: 'Are you sure?',
            text: 'You will not be able to revert this!',
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            confirmButtonText: 'Yes, delete it!',
            buttonsStyling: false
        }).then(function() {
            that.deleteBeneficiary(row);
            swal({
                title: 'Deleted!',
                text: 'Device has been deleted.',
                type: 'success',
                confirmButtonClass: 'btn btn-success',
                buttonsStyling: false
            });
        });

    }

}
