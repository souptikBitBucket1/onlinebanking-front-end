import {Routes} from "@angular/router";
import {AddBeneficiaryComponent} from "./add-beneficiary/add-beneficiary.component";
import {ManageBeneficiaryComponent} from "./manage-beneficiary/manage-beneficiary.component";

export const BeneficiaryRoutes: Routes = [
    {
        path: '',
        children: [
            {
                path: 'add',
                component: AddBeneficiaryComponent
            },
            {
                path: 'manage',
                component: ManageBeneficiaryComponent
            }
        ]
    }
];