import {BankDetails} from "./bankDetails";

export class User{
    id: number;
    name: string;
    email: string;
    password: string;
    balance: number;
    bankDetails: BankDetails;
}