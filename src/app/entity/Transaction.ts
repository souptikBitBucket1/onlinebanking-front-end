import {User} from './user';

export class Transaction{
    id: number;
    debitedUserAccount: User;
    creditedUserAccount: User;
}