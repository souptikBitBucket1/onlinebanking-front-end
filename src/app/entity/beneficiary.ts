import {User} from "./user";

export class Beneficiary{
    id: number;
    user: User;
    benefeciaryUser: User;
    transferLimit: number;
}